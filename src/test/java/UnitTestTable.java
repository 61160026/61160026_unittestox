/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.oxoop.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.mycompany.oxoop.Table;

/**
 *
 * @author Jirapat
 */
public class UnitTestTable {
    
    public UnitTestTable() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    public void testWinRow(){
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O,X);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());
    }
    public void testWinCol(){
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O,X);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());
    }
    public void testWinRow2(){
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O,X);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals(true, table.checkWin());
    }
    public void testWinCol2(){
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O,X);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals(true, table.checkWin());
    }
    public void testWinX1(){
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O,X);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testWinX2(){
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O,X);
        table.setRowCol(1, 3);
        table.setRowCol(2, 2);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());
    }
    public void testSwichPlayer() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.switchPlayer();
        assertEquals('x', table.getCurrentPlayer().getName());
    }
    public void testgetCurrentPlayer(){
        Player O = new Player('O');
        assertEquals('O',O.getName());
    }
    public void testgetCurrentPlayerX(){
        Player X = new Player('X');
        assertEquals('X',X.getName());
    }
    public void testWinCol3(){
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O,X);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testWinRow3(){
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O,X);
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    
}
